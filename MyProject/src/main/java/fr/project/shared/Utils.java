package fr.project.shared;

import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Utils {
	private final Random RANDOM = new SecureRandom();
	private final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	
	@Autowired
	ModelMapper modelMapper;
	

	public String generateStringId(int length) {
		StringBuilder returnValue = new StringBuilder(length);

		for (int i = 0; i < length; i++) {
			returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
		}

		return new String(returnValue);
	}
	
	public <T, U> List<U> mapperList( List<T> elements, List<U> dtos) {
		Type listType = new TypeToken<List<T>>(){}.getType();
		dtos = modelMapper.map(elements,listType);
		return dtos;
	}
}
