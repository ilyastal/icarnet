package fr.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.project.entities.Address;
import fr.project.entities.User;

public interface AddressRepository extends JpaRepository<Address, Long> {
	
	List<Address> findByUser(User user);
	Address findByAddressId(String addressId);

}
