package fr.project.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.project.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

	@Query(value = "SELECT * FROM users u WHERE u.email = ?1 ", nativeQuery = true)
	Optional<User> findByUserEmail(String email);

	Optional<User> findByUserId(String userId);

	Page<User> findAll(Pageable pageable);

	@Query(value = "SELECT * FROM users u WHERE u.first_name = ?1 OR u.last_name = ?1", nativeQuery = true)
	Page<User> findAllUserByCriteria(Pageable pageable, String search);
}
