package fr.project.controllers;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.project.dtos.requests.user.UserRequest;
import fr.project.dtos.responses.user.UserResponse;
import fr.project.dtos.user.UserDto;
import fr.project.services.UserService;
import jakarta.validation.Valid;



//  HTTP  request = > application server => application => <= filters => Adapter (Controller or Resource) => Model / View (HTML, SWing, etc), Json, XML / (MVC) 
// API => JSon/Xml/ hmtl

// View templating, jsp(x)/jstl, Swing/JSF


@RestController
@RequestMapping("/user")
public class UserResource {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(path = "/{id}", 
				produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserResponse> getUser(@PathVariable String id) {
		
		
		UserDto userDto = this.userService.getUserByUserId(id);
		
		return new ResponseEntity<UserResponse>(modelMapper.map(userDto, UserResponse.class) , HttpStatus.OK);
	}
	
	@GetMapping (produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE } )
	public ResponseEntity<List<UserResponse>> getAllUsers(@RequestParam(value = "page", defaultValue = "1") int page, 
														  @RequestParam(value = "limit", defaultValue = "15") int limit,
														  @RequestParam(value = "search", defaultValue = "") String search) {
		List<UserDto> userDtos = this.userService.getAllUsers(page, limit, search);
		Type listType = new TypeToken<List<UserResponse>>(){}.getType();
		List<UserResponse> users = modelMapper.map(userDtos,listType);
		return new ResponseEntity<List<UserResponse>>(users, HttpStatus.OK);
		
	}
	
	@PostMapping(	consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },
					produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserResponse> createUser(@RequestBody @Valid UserRequest userRequest) {
		UserDto userDto = this.userService.createUser(userRequest);
		return new ResponseEntity<UserResponse>(modelMapper.map(userDto, UserResponse.class), HttpStatus.CREATED);
	}
	
	@PutMapping(path = "/{id}",
				consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },
				produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserResponse> updateUser(@PathVariable String id, @RequestBody UserRequest userRequest) {
		UserDto userDto = this.userService.updateUser(id, userRequest);
		return new ResponseEntity<UserResponse>(modelMapper.map(userDto, UserResponse.class), HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping(path = "/{id}", 
				   produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> deleteUser(@PathVariable String id) {
		this.userService.deleteUser(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}

}
