package fr.project.controllers;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.project.dtos.AddressDto;
import fr.project.dtos.requests.AddressRequest;
import fr.project.dtos.responses.AddressResponse;
import fr.project.services.AddressService;

@RestController
@RequestMapping("/addresses")
public class AddressResource {

	@Autowired
	AddressService addressService;

	@Autowired
	ModelMapper modelMapper;

	@GetMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AddressResponse> getAddress(@PathVariable String id) {
		AddressDto addressDto = addressService.getAddress(id);
		AddressResponse addressResponse = modelMapper.map(addressDto, AddressResponse.class);
		return new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.OK);

	}

	@GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<AddressResponse>> getAddresses(Principal principal) {
		List<AddressDto> addressDtos = this.addressService.getAllAdesses(principal.getName());
		Type listType = new TypeToken<List<AddressResponse>>() {
		}.getType();
		List<AddressResponse> addressResponseList = modelMapper.map(addressDtos, listType);
		return new ResponseEntity<List<AddressResponse>>(addressResponseList, HttpStatus.OK);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AddressResponse> createAddress(@RequestBody AddressRequest addressRequest,
			Principal principal) {
		AddressDto addressDto = this.addressService.createAddress(addressRequest, principal.getName());
		AddressResponse addressResponse = modelMapper.map(addressDto, AddressResponse.class);
		return new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.CREATED);

	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AddressResponse> updateAddress(@PathVariable String id,
			@RequestBody AddressRequest addressRequest) {
		AddressDto addressDto = this.addressService.updateAddress(id, addressRequest);
		AddressResponse addressResponse = modelMapper.map(addressDto, AddressResponse.class);
		return new ResponseEntity<AddressResponse>(addressResponse, HttpStatus.ACCEPTED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAddress(@PathVariable String id) {
		this.addressService.deleteAddress(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}

}
