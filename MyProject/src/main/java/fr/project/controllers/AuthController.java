package fr.project.controllers;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.project.dtos.user.UserDto;
import fr.project.services.UserService;

@RestController
public class AuthController {
	@Autowired
	private JwtEncoder jwtEncoder;
	
    @Autowired
    UserService userService;

   
    @GetMapping("/token")
    public ResponseEntity<Map<String, String>> jwtToken(Authentication authentication){
    	
    	UserDto user = this.userService.getUserByEmail(authentication.getName());
        
    	String scope=authentication.getAuthorities()
                .stream().map(aut -> aut.getAuthority())
                .collect(Collectors.joining(" "));
        
        Instant instant=Instant.now();
        JwtClaimsSet jwtClaimsSet=JwtClaimsSet.builder()
                .subject(authentication.getName())
                .issuedAt(instant)
                .expiresAt(instant.plus(5, ChronoUnit.HOURS))
                .issuer("security-service")
                .claim("scope",scope)
                .claim("id", user.getUserId())
                .claim("name", user.getLastName() + " " + user.getFirstName())
                .build();
        
        
        //add in body
        Map<String, String> token = new HashMap<>();
        
        String jwtAccessToken=jwtEncoder.encode(JwtEncoderParameters.from(jwtClaimsSet)).getTokenValue();
        token.put("accessToken",jwtAccessToken);
        token.put("userId", user.getUserId());
        token.put("name", user.getLastName());
        token.put("firstName", user.getFirstName());
        token.put("civilite", user.getCivilite());
        
        // add in header
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtAccessToken);
        headers.add("userId", user.getUserId());
        headers.add("name", user.getCivilite() +". " + user.getFirstName() + " " + user.getLastName());
        
        return new ResponseEntity<>(token, headers, HttpStatus.OK);
       
    }
}
