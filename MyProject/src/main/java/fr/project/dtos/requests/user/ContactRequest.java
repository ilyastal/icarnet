package fr.project.dtos.requests.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ContactRequest {

	@NotBlank(message = "Ce champ ne doit pas être vide")
	private String phone;
	
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Email(message = "Ce champ doit respecter le format email")
	private String email;
}
