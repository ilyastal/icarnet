package fr.project.dtos.requests.user;

import java.util.List;
import java.util.Set;

import fr.project.dtos.requests.AddressRequest;
import fr.project.entities.Role;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {

	
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Min(value = 4, message = "Ce champ doit avoir au moins 4 caractères")
	private String password;
	
	@NotNull
	private String civilite;

	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Size(min = 3, message = "Ce champ doit avoir au moins 3 caractères")
	private String firstName;
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Size(min = 3, message = "Ce champ doit avoir au moins 3 caractères")
	private String lastName;
	
	@Valid
	private ContactRequest contact;
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	private Role role;
	
	private List<AddressRequest> addresses;
	
	private Set<TypesRequest> types;
	
}
