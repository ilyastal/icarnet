package fr.project.dtos.requests.user;

import java.util.List;

import fr.project.dtos.requests.AddressRequest;
import fr.project.entities.Role;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformationRequest {

	@NotNull
	private String civilite;

	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Size(min = 3, message = "Ce champ doit avoir au moins 3 caractères")
	private String firstName;
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Size(min = 3, message = "Ce champ doit avoir au moins 3 caractères")
	private String lastName;
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	@Email(message = "Ce champ ne doit respecter le format email")
	private String email;
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	private String phone;
	
	@NotNull(message = "Ce champ ne doit pas être nulle")
	private Role role;
	
	private List<AddressRequest> addresses;
}
