package fr.project.dtos.requests.user;

import lombok.Data;

@Data
public class TypesRequest {

	private String name;
}
