package fr.project.dtos.requests;

import fr.project.dtos.requests.user.UserRequest;
import lombok.Data;

@Data
public class AddressRequest {
	private String city;
	private String country;
	private String street;
	private String postalCode;
	private String type;
	private String addressId;
	private UserRequest user;
	
	
}
