package fr.project.dtos.user;

import java.util.List;

import fr.project.dtos.AddressDto;
import fr.project.entities.Role;
import lombok.Data;

@Data
public class InformationDto {
	
	private String civilite;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private Role role = Role.USER;
	private List<AddressDto> addresses;

}
