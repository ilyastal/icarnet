package fr.project.dtos.user;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class ContactDto implements Serializable {

	@Setter(value = AccessLevel.NONE)
	@Getter(value = AccessLevel.NONE)
	private static final long serialVersionUID = 6505542631044926044L;
	
	private String phone;
	private String email;
	

}
