package fr.project.dtos.user;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import fr.project.dtos.AddressDto;
import fr.project.entities.Role;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UserDto implements Serializable {
	@Setter(value = AccessLevel.NONE)
	@Getter(value = AccessLevel.NONE)
	private static final long serialVersionUID = -3616335527032152505L;
	
	private Long id;
	private String civilite;
	private String firstName;
	private String lastName;
	private String password;
	private String enryptedPassword;
	private String userId;
	private Role role = Role.USER;
	private List<AddressDto> addresses;
	private ContactDto contact;
	
	private Set<TypesDto> types;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEnryptedPassword() {
		return enryptedPassword;
	}

	public void setEnryptedPassword(String enryptedPassword) {
		this.enryptedPassword = enryptedPassword;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<AddressDto> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressDto> addresses) {
		this.addresses = addresses;
	}

	public ContactDto getContact() {
		return contact;
	}

	public void setContact(ContactDto contact) {
		this.contact = contact;
	}

	public Set<TypesDto> getTypes() {
		return types;
	}

	public void setTypes(Set<TypesDto> types) {
		this.types = types;
	}
	
	
	
		
}
