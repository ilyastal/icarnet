package fr.project.dtos.user;

import java.io.Serializable;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Data
public class TypesDto implements Serializable {

	@Setter(value = AccessLevel.NONE)
	@Getter(value = AccessLevel.NONE)
	private static final long serialVersionUID = 8769433545152006994L;
	
	private Long id;
	private String typeId;
	private String name;
	private Set<UserDto> users;

}
