package fr.project.dtos.responses;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class ErrorMessage {

	private Date timestmp;
	private String message;
	
	public ErrorMessage(Date timestmp, String message) {
		this.timestmp = timestmp;
		this.message = message;
	}
	
	
	
}
