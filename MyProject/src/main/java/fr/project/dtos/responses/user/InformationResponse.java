package fr.project.dtos.responses.user;

import java.util.List;

import fr.project.dtos.responses.AddressResponse;
import fr.project.entities.Role;
import lombok.Data;

@Data
public class InformationResponse {
	private String civilite;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private Role role;
	private List<AddressResponse> addresses;
}
