package fr.project.dtos.responses.user;

import lombok.Data;

@Data
public class TypesResponse {

	private String name;
	private String typeId;
}
