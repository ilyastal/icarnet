package fr.project.dtos.responses;

import lombok.Data;

@Data
public class AddressResponse {

	private String city;
	private String country;
	private String street;
	private String postalCode;
	private String type;
	private String addressId;
}
