package fr.project.dtos.responses.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactResponse {

	private String phone;
	private String email;

}
