package fr.project.dtos.responses.user;

import java.util.List;
import java.util.Set;

import fr.project.dtos.responses.AddressResponse;
import fr.project.entities.Role;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class UserResponse {

	
	private String userId;
	private String civilite;
	private String firstName;
	private String lastName;
	private Role role;
	private List<AddressResponse> addresses;
	private ContactResponse contact;
	private Set<TypesResponse> types;
	
}
