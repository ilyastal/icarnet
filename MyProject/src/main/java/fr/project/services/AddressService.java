package fr.project.services;

import java.util.List;

import fr.project.dtos.AddressDto;
import fr.project.dtos.requests.AddressRequest;

public interface AddressService {

	List<AddressDto> getAllAdesses(String name);
	
	AddressDto createAddress(AddressRequest addressRequest, String email);
	
	AddressDto getAddress(String id);
	
	AddressDto updateAddress(String id, AddressRequest addressRequest);
	void deleteAddress(String id);
}
