package fr.project.services;

import java.util.List;

import fr.project.dtos.requests.user.UserRequest;
import fr.project.dtos.user.UserDto;


public interface UserService {
	UserDto createUser(UserRequest userRequest);
	UserDto getUserByUserId(String userId);
	List<UserDto> getAllUsers(int page, int limit, String search);
	UserDto updateUser(String userId, UserRequest userRequest);
	UserDto getUserByEmail(String email);
	void deleteUser(String userId);
}
