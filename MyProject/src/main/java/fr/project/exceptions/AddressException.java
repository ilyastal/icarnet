package fr.project.exceptions;

public class AddressException extends RuntimeException {

	private static final long serialVersionUID = 8491122877053531817L;

	public AddressException(String message) {
		super(message);
	}

}
