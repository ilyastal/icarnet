package fr.project.exceptions;

public class UserException extends RuntimeException {

	private static final long serialVersionUID = 7296370632610427693L;
	
	public UserException(String message) {
		super(message);
	}


}
