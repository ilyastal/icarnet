package fr.project.servicesImpl;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.project.dtos.AddressDto;
import fr.project.dtos.requests.AddressRequest;
import fr.project.dtos.user.UserDto;
import fr.project.entities.Address;
import fr.project.entities.Role;
import fr.project.entities.User;
import fr.project.exceptions.AddressException;
import fr.project.repositories.AddressRepository;
import fr.project.repositories.UserRepository;
import fr.project.services.AddressService;
import fr.project.shared.Utils;
import jakarta.transaction.Transactional;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	AddressRepository addressRepository;
	@Autowired
	ModelMapper modelMapper;
	@Autowired
	UserRepository userRepository;
	@Autowired
	Utils utils;
	@Override
	public List<AddressDto> getAllAdesses(String email) {
		User user = this.userRepository.findByUserEmail(email).get();
		List<Address> addresses = user.getRole().equals(Role.ADMIN)? this.addressRepository.findAll(): this.addressRepository.findByUser(user);
		Type listType =  new TypeToken<List<AddressDto>>(){}.getType();
		List<AddressDto> addressDtos = modelMapper.map(addresses, listType);
		return addressDtos;
	}
	@Override
	public AddressDto createAddress(AddressRequest addressRequest, String email) {
		User currentUser = this.userRepository.findByUserEmail(email).get();
		UserDto userDto = modelMapper.map(currentUser, UserDto.class);
		AddressDto addressDto = modelMapper.map(addressRequest, AddressDto.class);
		//addressId
		addressDto.setAddressId(utils.generateStringId(32));
		// add addressDto to userDto
		userDto.getAddresses().add(addressDto);
		// set user to address
		addressDto.setUser(userDto);
		
		Address addressEntity = Address.builder()
										.addressId(addressDto.getAddressId())
										.street(addressDto.getStreet())
										.city(addressDto.getCity())
										.country(addressDto.getCountry())
										.postalCode(addressDto.getPostalCode())
										.type(addressDto.getType())
										.user(currentUser)
										.build();
		
		Address newAddress = this.addressRepository.save(addressEntity);
		AddressDto adDto = modelMapper.map(newAddress, AddressDto.class);
		
		return adDto;
	}
	@Override
	public AddressDto getAddress(String id) {
		Address addressEntity = this.addressRepository.findByAddressId(id);
		if (addressEntity == null ) throw new AddressException("L'addresse n'exite pas ");
		AddressDto addressDto = modelMapper.map(addressEntity, AddressDto.class);
		return addressDto;
	}
	@Override
	@Transactional
	public AddressDto updateAddress(String id, AddressRequest addressRequest) {
		Address addressEntity = this.addressRepository.findByAddressId(id);
		if (addressEntity == null ) throw new AddressException("L'addresse n'exite pas ");
		addressEntity.setCity(addressRequest.getCity());
		addressEntity.setCountry(addressRequest.getCountry());
		addressEntity.setPostalCode(addressRequest.getPostalCode());
		addressEntity.setStreet(addressRequest.getStreet());
		addressEntity.setType(addressRequest.getType());
		
		AddressDto addressDto = modelMapper.map(addressEntity, AddressDto.class);
		return addressDto;
	}
	@Override
	public void deleteAddress(String id) {
		Address addressEntity = this.addressRepository.findByAddressId(id);
		if (addressEntity == null ) throw new AddressException("L'addresse n'exite pas ");
		this.addressRepository.delete(addressEntity);
		
	}


	
}
