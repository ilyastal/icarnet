package fr.project.servicesImpl;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.project.dtos.requests.user.UserRequest;
import fr.project.dtos.user.TypesDto;
import fr.project.dtos.user.UserDto;
import fr.project.entities.User;
import fr.project.exceptions.UserException;
import fr.project.repositories.UserRepository;
import fr.project.services.UserService;
import fr.project.shared.Utils;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private  ModelMapper modelMapper;
	
	@Autowired
	private Utils utils;

	@Override
	public UserDto createUser(UserRequest userRequest) {
		//User userExiste = userRepository.findByEmail(userRequest.getEmail()).get();
		//if (userExiste != null) throw new UserException("user already exist!!!!!");
		
		//if (userRepository.findByUserEmail(userRequest.getContact().getEmail()) != null) throw new UserException("user already exist!!!!!");
		
		UserDto userDto =  modelMapper.map(userRequest, UserDto.class);
		// addresses
		for (int i = 0; i < userDto.getAddresses().size(); i++) {
			userDto.getAddresses().get(i).setAddressId(utils.generateStringId(32));
			userDto.getAddresses().get(i).setUser(userDto);
			
		}
		
		//groups
		Set<UserDto> users = new HashSet<UserDto>();
		users.add(userDto);
		for (TypesDto type : userDto.getTypes()) {
			type.setTypeId(utils.generateStringId(32));
			type.setUsers(users);
		}
		
		User newUser = modelMapper.map(userDto, User.class);
		newUser.setUserId(utils.generateStringId(32));
		newUser.setEnryptedPassword(passwordEncoder.encode(userDto.getPassword()));
		
		
		userRepository.save(newUser);
		userDto = modelMapper.map(newUser, UserDto.class);
		return userDto;
	}


	@Override
	public UserDto getUserByUserId(String userId) {
		User user = this.findUserById(userId);
		UserDto userDto = modelMapper.map(user, UserDto.class);
		return userDto;
	}

	@Override
	public UserDto updateUser(String userId, UserRequest userRequest) {
		User user = this.findUserById(userId);
		user.setFirstName(userRequest.getFirstName());
		user.setLastName(userRequest.getLastName());
		this.userRepository.save(user);
		UserDto userDto = modelMapper.map(user, UserDto.class);
		return userDto;
	}

	@Override
	public void deleteUser(String userId) {
		User user = this.findUserById(userId);
		this.userRepository.delete(user);
		
	}


	@Override
	public List<UserDto> getAllUsers(int page, int limit, String search) {
		//pour résoudre le problème de la page qui commence par 0 et en fornt on commence par 1
		if(page > 0) page -= 1;
		Pageable pageable = PageRequest.of(page, limit);
		Page<User>  usersPage = search.isEmpty() ? this.userRepository.findAll(pageable) : this.userRepository.findAllUserByCriteria(pageable, search);
		List<User> users = usersPage.getContent();
		
		Type listType = new TypeToken<List<UserDto>>(){}.getType();
		List<UserDto> userDtoList = modelMapper.map(users,listType);
		
		return userDtoList;
	}


	@Override
	public UserDto getUserByEmail(String email) {
		User user = this.userRepository.findByUserEmail(email)
										.orElseThrow( () -> new UsernameNotFoundException(email));;
		
		UserDto userDto = modelMapper.map(user, UserDto.class);
		return userDto;
	}
	
	public User findUserById(String id) {
		return  this.userRepository.findByUserId(id)
				.orElseThrow( () -> new UsernameNotFoundException(id));
	}

}
