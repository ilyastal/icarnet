package fr.project.entities;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "addresses")
@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class Address implements Serializable {
	@Setter(value = AccessLevel.NONE)
	@Getter(value = AccessLevel.NONE)
	private static final long serialVersionUID = -1788723784003955720L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false, length = 32)
	private String addressId;
	
	@Column(nullable = false, length = 50)
	private String city;
	
	@Column(nullable = false, length = 20)
	private String country;
	
	@Column(nullable = false, length = 50)
	private String street;
	
	@Column(nullable = false, length = 5)
	private String postalCode;
	
	@Column(nullable = false, length = 230)
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;


}
