package fr.project.entities;

public enum Civility {
	
	Mme ("Madame"),
	M ("Monsieur"),
	Mme_M ("Madame & Monsieur"),
	M_Mme ("Monsieur & Madame");
	
	 public final String name;

	private Civility(String name) {
		this.name = name;
	}
	

}
